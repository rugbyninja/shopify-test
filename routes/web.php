<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix'=>'/', 'middleware'=>'auth'], function(){
	
	// Returns the dashboard view
	Route::get('/', 'AdminController@view');

	// All orders routes
	Route::get('/orders', 'OrderController@index');
	Route::get('/orders/sync', 'OrderController@sync');
	Route::get('/orders/variant/{product_id}/{variant_id}', 'OrderController@variant');
	Route::get('/orders/customer/{id}', 'OrderController@customer');
});
