<div class="container">
	<div class="row">
		<div class="col-md-12">
			<a href="/orders" class="btn btn-success">Back to all orders</a>
		</div>
	</div>
</div>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default" data-widget="box-widget">
			  <div class="box-header">
			    <h3 class="box-title">All Variants Orders
			    	@if(isset(Order::variantId($product_id, $variant_id)[0]))
			    	|| {!! Order::variantId($product_id, $variant_id)[0]->product_name !!}
			    	@endif
			    </h3>
			    <div class="box-tools">
			    	<h3 class="box-title">Average All: {{ Order::meanVariant($product_id, $variant_id) }}</h3>
			      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
			    </div>
			  </div>
			  <div class="box-footer">
			    	<div class="row">
			    		<div class="col-md-1">
			    			<strong>Shopify ID</strong>
			    		</div>
			    		<div class="col-md-2">
			    			<strong>Customer</strong>
			    		</div>
			    		<div class="col-md-7">
			    			<strong>All Items In Order</strong>
			    		</div>
			    		<div class="col-md-2">
			    			<strong>Value</strong>
			    		</div>
			    	</div>
			    </div>
			  @foreach(Order::variantId($product_id, $variant_id) AS $order)
			    <div class="box-footer">
			    	<div class="row">
			    		<div class="col-md-1">
			    			{{ $order->shopify_id }}
			    		</div>
			    		<div class="col-md-2">
			    			<a href="/orders/customer/{{ $order->customer_id }}">{{ $order->customer_id }}</a>
			    		</div>
			    		<div class="col-md-7">
			    			<div class="col-md-3">
			    				<strong>Product Name</strong>
			    			</div>
			    			<div class="col-md-3">
			    				<strong>Variant</strong>
			    			</div>
			    			<div class="col-md-3">
			    				<strong>Quantity</strong>
			    			</div>
			    			<div class="col-md-3">
			    				<strong>Price</strong>
			    			</div>
			    			@foreach(Order::items($order->shopify_id) AS $item)
			    			<div class="col-md-3">
			    				<a href="/orders/variant/{{ $item->product_id }}/{{ $item->variant_id }}">{!! $item->product_name !!}</a>
			    			</div>
			    			<div class="col-md-3">
			    				<a href="/orders/variant/{{ $item->product_id }}/{{ $item->variant_id }}">{!! $item->variant_name !!}</a>
			    			</div>
			    			<div class="col-md-3">
			    				{!! $item->quantity !!}
			    			</div>
			    			<div class="col-md-3">
			    				£{!! $item->value !!}
			    			</div>
			    			@endforeach
			    		</div>
			    		<div class="col-md-2">
			    			{{ Order::value($order->shopify_id) }}
			    		</div>
			    	</div>
			  	</div>
			  @endforeach
			</div>
		</div>
	</div>
</div>