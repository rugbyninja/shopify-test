@if(Session::has('success'))
<div class="alert alert-success">
    {!! Session::get('success') !!}
</div>
@endif
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<a href="/orders/sync" class="btn btn-success">Run Orders Sync</a>
		</div>
	</div>
</div>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default" data-widget="box-widget">
			  <div class="box-header">
			    <h3 class="box-title">Customers Orders</h3>
			    <div class="box-tools">
			    	<h3 class="box-title">Average All: {{ Order::meanAll() }}</h3>
			      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
			    </div>
			  </div>
			  <div class="box-footer">
			    	<div class="row">
			    		<div class="col-md-4">
			    			<strong>Email</strong>
			    		</div>
			    		<div class="col-md-4">
			    			<strong>Average for Customer</strong>
			    		</div>
			    		<div class="col-md-4">
			    			<strong>Actions</strong>
			    		</div>
			    	</div>
			    </div>
			  @foreach(Order::allCustomers() AS $order)
			    <div class="box-footer">
			    	<div class="row">
			    		<div class="col-md-4">
			    			{{ $order->customer_id }}
			    		</div>
			    		<div class="col-md-4">
			    			{{ Order::meanCustomer($order->customer_id) }}
			    		</div>
			    		<div class="col-md-4">
			    			<a class="btn btn-primary" href="/orders/customer/{{ $order->customer_id  }}"><i class="fa fa-eye"></i></a>
			    		</div>
			    	</div>
			  	</div>
			  @endforeach
			</div>

			<div class="box box-default" data-widget="box-widget">
			  <div class="box-header">
			    <h3 class="box-title">Variant Figures</h3>
			    <div class="box-tools">
			      <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
			    </div>
			  </div>
			  <div class="box-footer">
			    	<div class="row">
			    		<div class="col-md-4">
			    			<strong>Variant Name</strong>
			    		</div>
			    		<div class="col-md-4">
			    			<strong>Average for Variant</strong>
			    		</div>
			    		<div class="col-md-4">
			    			<strong>Actions</strong>
			    		</div>
			    	</div>
			    </div>
			  @foreach(Order::allVariants() AS $order)
			    <div class="box-footer">
			    	<div class="row">
			    		<div class="col-md-4">
			    			{{ $order->product_name }} - {{ $order->variant_name }}
			    		</div>
			    		<div class="col-md-4">
			    			{{ Order::meanVariant($order->product_id, $order->variant_id) }}
			    		</div>
			    		<div class="col-md-4">
			    			<a class="btn btn-primary" href="/orders/variant/{{ $order->product_id }}/{{ $order->variant_id }}"><i class="fa fa-eye"></i></a>
			    		</div>
			    	</div>
			  	</div>
			  @endforeach
			</div>
		</div>
	</div>
</div>