<a href="/orders" class="btn btn-success">View All Orders</a>
<h2>Key Financials</h2>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="info-box">
				<span class="info-box-icon bg-green"><i class="fa fa-gbp"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Total Orders</span>
					<span class="info-box-number">{!! Order::countAll(); !!}</span>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="info-box">
				<span class="info-box-icon bg-green"><i class="fa fa-gbp"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Total Value</span>
					<span class="info-box-number">{!! Order::valueAll(); !!}</span>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="info-box">
				<span class="info-box-icon bg-green"><i class="fa fa-gbp"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Average Value</span>
					<span class="info-box-number">{!! Order::meanAll() !!}</span>
				</div>
			</div>
		</div>
	</div>
</div>
<h2>Average Variant Sale Figures</h2>
<div class="container">
	<div class="row">
		@foreach(Order::allVariants() AS $order)
		<div class="col-md-3">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">{!! $order->product_name !!}</span>
					<span class="small">{!! $order->variant_name !!}</span>
					<span class="info-box-number">{!! Order::meanVariant($order->product_id, $order->variant_id) !!}</span>
					<span class="small"><a href="/orders/variant/{{ $order->product_id }}/{{$order->variant_id }}">see more ></a></span>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>





