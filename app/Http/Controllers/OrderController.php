<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    public $crud = 'admin.crud.orders.';

    /**
     * Returns the index view of the crud item
     * @return type
     */
    public function index()
    {
    	$view = $this->crud.'index';
    	return view('view')->with('view', $view);
    }

    /**
     * Returns an overview of a specified customer
     * @param type $id 
     * @return type
     */
    public function customer($id)
    {
        $view = $this->crud.'customer';
        return view('view')->with('view', $view)
                           ->with('id', $id);
    }

    /**
     * Returns an overview of a specified variant
     * @param type $id 
     * @return type
     */
    public function variant($product_id, $variant_id)
    {
        $view = $this->crud.'variant';
        return view('view')->with('view', $view)
                           ->with('product_id', $product_id)
                           ->with('variant_id', $variant_id);
    }

    /**
     * Syncs all shopify orders into the database
     * @return type
     */
    public function sync()
    {
    	$success = 0;
    	$error = 0;

        $last = Order::orderBy('shopify_id', 'desc')->first();
        if($last){
            $since = '&since_id='.$last->shopify_id;
        } else{
            $since = '';
        }

    	$url = 'https://b36ad51b5e21644606199a43876f0d23:1a31048b5df675f8390f7ac0f569efbb@technical-zeta-psi.myshopify.com/admin/orders.json?status=any&limit=250';

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);

        $data = json_decode($output);

        foreach($data->orders AS $key => $value){
        	if($this->construct($value)){
        		$success++;
        	} else{
        		$error++;
        	}
        }

        return redirect()->back()->with('success', $success.' orders successfully synced with '.$error.' errors');

    	// return redirect()->back()->with('success', $success.' orders successfully synced with '.$error.'errors');
    }

    /**
     * Creates a new order for each line item
     * @param type $data 
     * @return type
     */
    public function construct($data)
    {
    	try{
	    	foreach($data->line_items AS $key => $item){
		    	$return['shopify_id'] = $data->id;
                $return['customer_id'] = $data->contact_email;
		    	$return['product_id'] = $item->product_id;
		    	$return['product_name'] = $item->title;
		    	$return['variant_id'] = $item->variant_id;
		    	$return['variant_name'] = $item->variant_title;
		    	$return['value'] = $item->price;
		    	$return['quantity'] = $item->quantity;
		    	$return['order_date'] = $data->created_at;
		    	$return['status'] = $data->financial_status;

		    	Order::create($return);	
	    	}
	    	return true;
	    } catch(\QueryException $e){
	    	return false;
	    }
    }
}
