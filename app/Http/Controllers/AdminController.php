<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

	public $crud = 'admin.dashboard.';

	/**
	 * Retrievs a static page view based on the slug set in the browser
	 * @param type $slug 
	 * @return type
	 */
    public function view()
    {
    	$view = $this->crud.'index';
    	return view('view')->with('view', $view);
    }
}
