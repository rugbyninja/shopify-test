<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
    	'shopify_id', 'product_id', 'product_name', 'variant_id', 'variant_name', 'value', 'order_date', 'status', 'quantity', 'customer_id',
    ];

    /**
     * Gets the average of all orders
     * @return type
     */
    public static function meanAll()
    {
    	try{
    	$orders = Order::select('shopify_id')->where('status', 'paid')->orWhere('status', 'fulfilled')->groupBy('shopify_id')->get();
    	$count = count($orders);

    	$value = Order::where('status', 'paid')->orWhere('status', 'fulfilled')->sum('value');

    	return '£'.number_format($value/$count,2);
    	} catch(\ErrorException $e){
	    		return '£0';
	    }
    }

    /**
     * Returns a count of all orders
     * @return type
     */
    public static function countAll()
    {
        try{
        $orders = Order::select('shopify_id')->where('status', 'paid')->orWhere('status', 'fulfilled')->groupBy('shopify_id')->get();
        $count = count($orders);

        return $count;
        } catch(\ErrorException $e){
                return '0';
        }

    }

    /**
     * Returns a sum of entire value of all orders placed
     * @return type
     */
    public static function valueAll()
    {
        return '£'.number_format(Order::where('status', 'paid')->orWhere('status', 'fulfilled')->sum('value'),2);
    }

    /**
     * Returns the mean value of a specified customers order
     * @param type $id 
     * @return type
     */
    public static function meanCustomer($id)
    {
    	try{
    	$orders = Order::select('customer_id', 'shopify_id')->whereIn('status', ['paid', 'fulfilled'])->where('customer_id', $id)->groupBy('shopify_id')->get();
    	$count = count($orders);

    	$value = Order::where('customer_id', $id)->whereIn('status', ['paid', 'fulfilled'])->sum('value');

    	return '£'.number_format($value/$count,2);
    	} catch(\ErrorException $e){
	    		return '£0';
	    	}
    }

    /**
     * Returs the mean value of a specified variant from orders
     * @param type $id 
     * @return type
     */
    public static function meanVariant($product_id, $variant_id)
    {
    	try{
        $value = 0;
    	
        $orders = Order::select('variant_id', 'shopify_id')->whereIn('status', ['paid', 'fulfilled'])->where([['product_id', $product_id],['variant_id', $variant_id]])->groupBy('shopify_id')->get();
    	$count = count($orders);

        foreach($orders AS $order){
            $value = $value+Order::where('shopify_id', $order->shopify_id)->sum('value');
        }

    	return '£'.number_format($value/$count,2);
    	} catch(\ErrorException $e){
	    		return '£0';
	    	}
    }

    /**
     * Return orders grouped by customer
     * @return type
     */
    public static function allCustomers()
    {
    	return Order::groupBy('customer_id')->orderBy('created_at', 'desc')->get();
    }

    /**
     * Return orders grouped by variant
     * @return type
     */
    public static function allVariants()
    {
    	return Order::groupBy('product_id')->groupBy('variant_id')->orderBy('product_name', 'asc')->get();
    }

    /**
     * Returns all orders by a specified customer
     * @param type $id 
     * @return type
     */
    public static function customerId($id)
    {
    	return Order::where('customer_id', $id)->groupBy('shopify_id')->orderBy('created_at', 'desc')->get();
    }

    /**
     * Returns all orders by a specified variant
     * @param type $id 
     * @return type
     */
    public static function variantId($product_id, $variant_id)
    {
    	return Order::where([['product_id', $product_id],['variant_id', $variant_id]])->orderBy('created_at', 'desc')->get();
    }

    /**
     * Returns all items within an order
     * @param type $id 
     * @return type
     */
    public static function items($id)
    {
    	return Order::where('shopify_id', $id)->groupBy('variant_id')->get();
    }

    /**
     * Returns the value of a given order
     * @param type $id 
     * @return type
     */
    public static function value($id)
    {
    	return '£'.number_format(Order::where('shopify_id', $id)->sum('value'),2);
    }
}
